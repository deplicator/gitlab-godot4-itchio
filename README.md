# GitLab CD/CD: Godot4 -> Itch.io

This is an example project with a GitLab CI/CD workflow to push a project to [Itch.io](itch.io). The
project will be playable on Itch.io at
[deplicator.itch.io/gitlab-godot4-itchio](https://deplicator.itch.io/gitlab-godot4-itchio) and have
a download for Linux, MacOS, and Windows.

![Running on Itch.io](docs/goal.png)

## Setup

### Godot

The name of the Godot project and Itch.io project URL are the same. They contain no spaces or
invalid URL characters. They don't have to be this way, but the ci yaml script uses [a variable
called
`PROJECT_NAME`](https://gitlab.com/deplicator/gitlab-godot4-itchio/-/blob/eec3ddc6428b8c2b2b07f3847d3363a7f301afff/.gitlab-ci.yml#L8)
that relies on the consistency.

In Godot Exports, [the export
names](https://gitlab.com/deplicator/gitlab-godot4-itchio/-/blob/eec3ddc6428b8c2b2b07f3847d3363a7f301afff/export_presets.cfg#L3)
use the same name as [Itch.io Channels](https://itch.io/docs/butler/pushing.html#channel-names). The
script uses the channel names `html`, `linux`, `macos`, and `windows` for
[building](https://gitlab.com/deplicator/gitlab-godot4-itchio/-/blob/eec3ddc6428b8c2b2b07f3847d3363a7f301afff/.gitlab-ci.yml#L37)
and
[pushing](https://gitlab.com/deplicator/gitlab-godot4-itchio/-/blob/eec3ddc6428b8c2b2b07f3847d3363a7f301afff/.gitlab-ci.yml#L72)
to Itch.io.

Export path is `export/<project name>-<channel>.{zip|exe}`

- This example uses `exports/gitlab-godot4-itch.io-linux.zip` for linux
- `html` is an exception, it will be `exports/html/index.html`, details in Workflow section below

![Linux export example](docs/linux-export.png)

### Itch.io

Setup a project in Itch.io with the same url name as the Godot project.

- Change **Kind of project** to HTML.

Under **Embed options**:

- Change the **Viewport dimensions** to match the game
  - This project uses Godot defaults of 1152x648
- Check the box next to **SharedArrayBuffer support**
  - _Note:_ This will not be playable on Safari, it's a [known
issue](https://docs.godotengine.org/en/stable/tutorials/export/exporting_for_web.html).
  - _Godot 4's HTML5 exports currently cannot run on macOS and iOS due to upstream bugs with
    SharedArrayBuffer and WebGL 2.0._
  - It looks like [a fix is coming in Godot
    4.3](https://godotengine.org/article/dev-snapshot-godot-4-3-dev-3/#single-threaded-web-exports)!

Get API Key by going to **User dropdown -> Settings -> API keys**. Generate a new API key or copy an
existing one. It will be used in the GitLab section next.

### GitLab

Add two variables to the repository by going to **Settings -> CI/CD -> Variables (expand section) ->
Add variable**. Most settings can be ignored, only `key` and `value` are needed.

| key               | value |
| ----------------- | ----- |
| `BUTLER_API_KEY`  | API key from Itch.io (copied in previous section) |
| `ITCHIO_USERNAME` | Your Itch.io username, this doesn't have to be a secret, [it can be a variable the script](https://gitlab.com/deplicator/gitlab-godot4-itchio/-/blob/eec3ddc6428b8c2b2b07f3847d3363a7f301afff/.gitlab-ci.yml#L8) |

## Using

The workflow defined in `.gitlab-ci.yml` in run manually.

It is started by navigating to **Build -> Pipelines**. If a pipeline exists (pictured below) click
the play button (circled in red below) then choose `build-job`. If no pipeline exists yet, click
**RUN pipeline** to create one first. The pipeline will start running the build job, then the deploy
job will happen when it is completed.

![Gitlab Build Pipeline](docs/start-pipeline.png)

### Build Job

Builds the Godot project for four platforms; Linux, MacOS, Web, and Windows.

- Downloads Godot and Export Templates
- Build 4 versions of the project
  - Zip file for Linux and MacOS
  - Exe for Windows
  - Several files for Web, they all get [compressed into a
    zip](https://gitlab.com/deplicator/gitlab-godot4-itchio/-/blob/eec3ddc6428b8c2b2b07f3847d3363a7f301afff/.gitlab-ci.yml#L42)

### Publish Job

[Pushes
everything](https://gitlab.com/deplicator/gitlab-godot4-itchio/-/blob/eec3ddc6428b8c2b2b07f3847d3363a7f301afff/.gitlab-ci.yml#L72)
to Itch.io using [Butler](https://itch.io/docs/butler/).

### Completion

When done, the project will be updated with the version built from this repository. Itch.io will
show a notification.

![Itch.io notification](docs/itchio-notificaiton.png)

## Related

- [GitHub Actions: Godot4 -> Itch.io](https://github.com/deplicator/github-godot4-itchio)
- [Gitea Actions: Godot4 -> Itch.io](https://gitea.com/deplicator/gitea-godot4-itchio)
- [A Related Blog Post](https://deplicator.gitlab.io/posts/2024/2-devops-for-indie-games/)
- [And another Related Blog Post](https://deplicator.gitlab.io/posts/2024/3-more-game-related-devops/)
